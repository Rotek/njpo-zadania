package kolejcje2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AdressBook {
	private List<Person> persons = new ArrayList<Person>();

	public List<Person> getPersons() {
		return persons;
	}

	public void addPerson(Person person) {
		persons.add(person);
	}

	public void sortPersons(Comparator<Person> comparator) {
		persons.sort(comparator);
	}
}

package kolejcje2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.awt.event.ActionEvent;

public class MainJframe extends JFrame {

	private JPanel contentPane;
	private AdressBook adressBook = new AdressBook();
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJframe frame = new MainJframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJframe() {
		adressBook.addPerson(new Person("darek", "roterman", "123123214"));
		adressBook.addPerson(new Person("marek", "roterman", "123123214"));
		adressBook.addPerson(new Person("darek", "roterman", "123123213"));
		adressBook.addPerson(new Person("darek", "Poterman", "123123214"));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textArea = new JTextArea();
		textArea.setBounds(10, 11, 414, 239);
		contentPane.add(textArea);
		updateTextArea();

		JButton btnSort = new JButton("sort");
		btnSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adressBook.sortPersons(new PersonLexicographicalComparator());
				updateTextArea();
			}
		});
		btnSort.setBounds(10, 256, 89, 23);
		contentPane.add(btnSort);
	}

	private void updateTextArea() {
		StringBuilder personsInText = new StringBuilder();
		for (Person person : adressBook.getPersons()) {
			personsInText.append(person + "\n");
		}
		textArea.setText(personsInText.toString());
	}
}

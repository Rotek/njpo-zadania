package kolekcje3;

import java.util.List;

public abstract class ListBenchmark {

	protected String testName;

	protected abstract void executeOneAction(List<Object> list);

	public ListBenchmark(String testName) {
		this.testName = testName;
	}

	public void testAndPrint(List<Object> list1, String list1name, List<Object> list2, String list2name) {
		long difference = test(list1, list2);
		String winner;
		if (difference > 0) {
			winner = list2name;
		} else if (difference < 0) {
			winner = list1name;
			difference = -difference;
		} else {
			winner = "both - draw";
		}
		System.out.println("Competition \"" + testName + "\" won by " + winner + " by " + difference + "ms");
	}

	public long test(List<Object> list1, List<Object> list2) {
		long result1 = testOne(list1);
		long result2 = testOne(list2);
		long difference = result1 - result2;
		return difference;
	}

	protected long testOne(List<Object> list) {
		long startTime = System.currentTimeMillis();
		executeOneAction(list);
		long endTime = System.currentTimeMillis();
		return endTime - startTime;
	}

}
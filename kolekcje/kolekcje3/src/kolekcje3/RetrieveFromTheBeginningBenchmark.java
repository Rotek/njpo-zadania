package kolekcje3;

import java.util.List;

public class RetrieveFromTheBeginningBenchmark extends ListBenchmark {

	public RetrieveFromTheBeginningBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.get(0);
		}
	}

}

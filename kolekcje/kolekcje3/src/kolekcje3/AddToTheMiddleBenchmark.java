package kolekcje3;

import java.util.List;

public class AddToTheMiddleBenchmark extends ListBenchmark {

	public AddToTheMiddleBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.add(list.size() / 2, new Object());
		}
	}

}

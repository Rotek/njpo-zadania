package kolekcje3;

import java.util.List;

public class DeleteAtTheBeginningBenchmark extends ListBenchmark {

	public DeleteAtTheBeginningBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.remove(0);
		}
	}

}

package kolekcje3;

import java.util.List;

public class AddToTheEndBenchmark extends ListBenchmark {

	public AddToTheEndBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 1000000; i++) {
			list.add(list.size()-1,new Object());
		}
	}

}

package kolekcje3;

import java.util.List;

public class AddToTheBeginingBenchmark extends ListBenchmark {

	public AddToTheBeginingBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.add(0, new Object());
		}
	}

}

package kolekcje3;

import java.util.List;

public class RetrieveFromTheEndBenchmark extends ListBenchmark {

	public RetrieveFromTheEndBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.get(list.size()-1);
		}
	}

}

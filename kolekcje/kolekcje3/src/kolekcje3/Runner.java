package kolekcje3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Runner {
	private static final String LINKED_LIST_NAME = "linked list";
	private static final String ARRAY_LIST_NAME = "array list";
	static List<Object> arrayList = new ArrayList<Object>();
	static List<Object> linkedList = new LinkedList<Object>();

	public static void main(String[] args) {
		List<ListBenchmark> benchmarks = new ArrayList<ListBenchmark>();
		benchmarks.add(new AddToTheBeginingBenchmark("adding to the beginning"));
		benchmarks.add(new AddToTheMiddleBenchmark("adding to the middle"));
		benchmarks.add(new AddToTheEndBenchmark("adding to the end"));
		benchmarks.add(new DeleteAtTheBeginningBenchmark("removing from beginning"));
		benchmarks.add(new DeleteFromTheMiddleBenchmark("removing from the middle"));
		benchmarks.add(new DeleteAtTheEndBenchmark("removing from the end"));
		benchmarks.add(new RetrieveFromTheBeginningBenchmark("retrieving from beginning"));
		benchmarks.add(new RetrieveFromTheMiddleBenchmark("retrieving from the middle"));
		benchmarks.add(new RetrieveFromTheEndBenchmark("retrieving from the end"));

		for (ListBenchmark benchmark : benchmarks) {
			benchmark.testAndPrint(arrayList, ARRAY_LIST_NAME, linkedList, LINKED_LIST_NAME);
		}

	}

}

package kolekcje3;

import java.util.List;

public class DeleteAtTheEndBenchmark extends ListBenchmark {

	public DeleteAtTheEndBenchmark(String testName) {
		super(testName);
	}

	@Override
	protected void executeOneAction(List<Object> list) {
		for (int i = 0; i < 10000; i++) {
			list.remove(list.size()-1);
		}
	}

}

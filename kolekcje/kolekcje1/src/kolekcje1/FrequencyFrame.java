package kolekcje1;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class FrequencyFrame extends JFrame {

	private JPanel contentPane;
	private JTextArea textArea;

	public JTextArea getTextArea() {
		return textArea;
	}

		/**
	 * Launch the application.
	 */
		/**
	 * Create the frame.
	 */
	public FrequencyFrame() {
		setBounds(450, 100, 228, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textArea = new JTextArea();
		textArea.setBounds(10, 11, 91, 239);
		contentPane.add(textArea);
		
	}
}

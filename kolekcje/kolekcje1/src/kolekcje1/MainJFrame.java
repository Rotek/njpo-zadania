package kolekcje1;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class MainJFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextArea textArea;
	private FrequencyFrame frequencyFrame = new FrequencyFrame();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJFrame frame = new MainJFrame();
					frame.setVisible(true);
					frame.frequencyFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 360, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnOtworz = new JButton("otworz");
		btnOtworz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frequencyFrame.setVisible(true);
			}
		});
		btnOtworz.setBounds(153, 10, 100, 23);
		contentPane.add(btnOtworz);

		JButton btnZamknij = new JButton("zamknij");
		btnZamknij.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frequencyFrame.setVisible(false);
			}
		});
		btnZamknij.setBounds(153, 44, 100, 23);
		contentPane.add(btnZamknij);

		textField = new JTextField();
		textField.setToolTipText("wpisz sciezke do pliku");
		textField.setBounds(153, 92, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnWczytajPlik = new JButton("wczytaj plik");
		btnWczytajPlik.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String path = textField.getText();
				try {
					String content = readFile(path, Charset.defaultCharset());
					textArea.setText(content);
					HashMap<Character, Integer> charactersFrequency = getCharactersFrequency(content);
					for (Map.Entry<Character, Integer> pair : charactersFrequency.entrySet()) {
						frequencyFrame.getTextArea().append(pair.getKey() + ": " + pair.getValue() + "\n");
					}

				} catch (IOException e) {
					JOptionPane.showMessageDialog(textField, "something is worng with that file");
				}
			}
		});
		btnWczytajPlik.setBounds(153, 133, 89, 23);
		contentPane.add(btnWczytajPlik);

		JLabel lblSciezka = new JLabel("sciezka");
		lblSciezka.setBounds(153, 78, 46, 14);
		contentPane.add(lblSciezka);

		textArea = new JTextArea();
		textArea.setBounds(10, 14, 133, 236);
		contentPane.add(textArea);
		textArea.setEditable(false);
	}

	private String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	private HashMap<Character, Integer> getCharactersFrequency(String text) {
		HashMap<Character, Integer> frequencies = new HashMap<Character, Integer>();
		for (int i = 0; i < text.length(); i++) {
			Character character = text.charAt(i);
			if (!frequencies.containsKey(character)) {
				frequencies.put(character, 1);
			} else {
				int frequency = frequencies.get(character);
				frequencies.put(character, frequency + 1);
			}
		}
		return frequencies;
	}
}

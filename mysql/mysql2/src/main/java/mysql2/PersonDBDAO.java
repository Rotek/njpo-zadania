package mysql2;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonDBDAO implements PersonDAO {

	@Override
	public List<Person> getPersons() {
		ResultSet resultSet = DBSingleton.INSTANCE.executeQuery("select* from ksiazkaadresowa");
		List<Person> persons = new ArrayList<Person>();
		try {
			while (resultSet.next()) {
				persons.add(new Person(resultSet.getString(2), resultSet.getString(3), resultSet.getString(4)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("BLOND");
		}
		return persons;
	}

	@Override
	public void addPerson(Person person) {
		String query = "insert into ksiazkaadresowa values(null,\"" + person.getName() + "\",\"" + person.getSurname() + "\",\""
				+ person.getPhoneNumber() + "\");";
		DBSingleton.INSTANCE.executeUpdate(query);
	}

}

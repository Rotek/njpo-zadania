package mysql2;

import java.util.Comparator;

public class PersonLexicographicalComparator implements Comparator<Person> {

	@Override
	public int compare(Person person1, Person person2) {
		int nameCompareResult = person1.getName().compareToIgnoreCase(person2.getName());
		if (nameCompareResult != 0) {
			return nameCompareResult;
		} else {
			int surnameCompareResult = person1.getSurname().compareToIgnoreCase(person2.getSurname());
			if (surnameCompareResult != 0) {
				return surnameCompareResult;
			} else {
				int phoneNumberCompareResult = person1.getPhoneNumber().compareToIgnoreCase(person2.getPhoneNumber());
				return phoneNumberCompareResult;
			}
		}
	}

}

package mysql2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class MainJframe extends JFrame {

	private JPanel contentPane;
	private AdressBook adressBook = new AdressBook();
	private JTextArea textArea;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJframe frame = new MainJframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJframe() {
		adressBook.addPerson(new Person("darek", "roterman", "123123214"));
		adressBook.addPerson(new Person("marek", "roterman", "123123214"));
		adressBook.addPerson(new Person("darek", "roterman", "123123213"));
		adressBook.addPerson(new Person("darek", "Poterman", "123123214"));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textArea = new JTextArea();
		textArea.setBounds(10, 11, 414, 239);
		contentPane.add(textArea);

		textField = new JTextField();
		textField.setBounds(10, 259, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(106, 259, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(202, 259, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnDodaj = new JButton("dodaj");
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String imie = textField.getText();
				String nazwisko = textField_1.getText();
				String tel = textField_2.getText();
				adressBook.addPerson(new Person(imie, nazwisko, tel));
				updateTextArea();
			}
		});
		btnDodaj.setBounds(298, 258, 89, 23);
		contentPane.add(btnDodaj);

		JLabel lblImie = new JLabel("imie");
		lblImie.setBounds(10, 247, 46, 14);
		contentPane.add(lblImie);

		JLabel lblNaziwsko = new JLabel("naziwsko");
		lblNaziwsko.setBounds(106, 247, 46, 14);
		contentPane.add(lblNaziwsko);

		JLabel lblTel = new JLabel("tel");
		lblTel.setBounds(203, 247, 46, 14);
		contentPane.add(lblTel);
		updateTextArea();
	}

	private void updateTextArea() {
		StringBuilder personsInText = new StringBuilder();
		for (Person person : adressBook.getPersons()) {
			personsInText.append(person + "\n");
		}
		textArea.setText(personsInText.toString());
	}
}

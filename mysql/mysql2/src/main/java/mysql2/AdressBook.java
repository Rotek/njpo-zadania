package mysql2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AdressBook implements PersonDAO {
	PersonDAO personDAO = new PersonDBDAO();

	public List<Person> getPersons() {
		return personDAO.getPersons();

	}

	public void addPerson(Person person) {
		personDAO.addPerson(person);
	}

}

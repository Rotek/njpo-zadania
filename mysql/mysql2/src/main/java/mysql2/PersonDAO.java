package mysql2;

import java.util.List;

public interface PersonDAO {

	public List<Person> getPersons();

	public void addPerson(Person person);
}

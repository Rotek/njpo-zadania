package mysql3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public enum DBSingleton {
	INSTANCE;

	private static final String url = "jdbc:mysql://127.0.0.1:3306/bazatestowa";
	private static final String user = "testuser";
	private static final String password = "1";

	private Connection connection;

	private DBSingleton() {
		try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
		}
	}

	ResultSet executeQuery(String query) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			return preparedStatement.executeQuery();
		} catch (SQLException e) {
		}
		return null;
	}

	int executeUpdate(String query) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}

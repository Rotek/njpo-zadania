package mysql3;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.lang.model.type.DeclaredType;

public class Runner {

	private static final String sortedQuery = "select id,data from benchamrk order by data";
	private static final String unsortedQuery = "select id,data from benchamrk";

	public static void main(String[] args) {
		System.out.println("czas dla sql: " + testSQLsorting() + "ms");
		System.out.println("czas dla javy: " + testSQLsorting() + "ms");
	}

	private static long testSQLsorting() {
		long startTime = System.currentTimeMillis();
		DBSingleton.INSTANCE.executeQuery(sortedQuery);
		long endTime = System.currentTimeMillis();
		return (endTime - startTime);

	}

	private static long testJavasorting() {
		long startTime = System.currentTimeMillis();
		List<Krotka> objects = new ArrayList<Krotka>();
		ResultSet rs = DBSingleton.INSTANCE.executeQuery(unsortedQuery);
		try {
			while (rs.next()) {
				Krotka krotka = new Krotka(rs.getInt(1), rs.getInt(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Collections.sort(objects);
		long endTime = System.currentTimeMillis();
		return (endTime - startTime);
	}

}
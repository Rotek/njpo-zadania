package mysql3;

public class Krotka implements Comparable<Krotka> {
	int id;
	int data;

	public Krotka(int id, int data) {
		super();
		this.id = id;
		this.data = data;
	}

	@Override
	public int compareTo(Krotka o) {
		return data - o.data;
	}

}

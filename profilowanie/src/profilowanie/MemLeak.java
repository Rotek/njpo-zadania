package profilowanie;

import java.util.HashMap;
import java.util.Map;

public class MemLeak {

	public final String key;

	public MemLeak(String key) {
		this.key = key;
	}

	public static void main(String[] args) {
		// nalezy dodac limit
		// properties to static field, nalezy dodac wlasn� mape
		try {
			Map<MemLeak, String> map = new HashMap<MemLeak, String>();

			int limit = 100;

			for (int i = 0; i < limit; i++) {

				map.put(new MemLeak("key"), "value");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// skoro u�ywamy hashmapy nalezy nadpisa� metode hashCode
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemLeak other = (MemLeak) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

}

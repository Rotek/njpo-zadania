package watki1;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Runner {
	private static final int NUMBER_OF_THREAD = 10;
	private static final int BYTES_LENGTH = 1000000;
	private static byte[] bytes = new byte[BYTES_LENGTH];
	public static AtomicInteger atomicCounter = new AtomicInteger(0);

	public static synchronized byte[] getBytes() {
		return bytes;
	}

	public static void main(String[] args) {
		initBytes();
		Scanner scanner = new Scanner(System.in);
		System.out.println("kliknij enter aby wygenerowac watki");
		if (scanner.hasNextLine()) {
			scanner.nextLine();
			for (int i = 0; i < NUMBER_OF_THREAD; i++) {
				new ZipThread().start();
			}
		}
		scanner.hasNextLine();
		scanner.close();

	}

	private static void initBytes() {
		for (int i = 0; i < BYTES_LENGTH; i++) {
			bytes[i] = 1;
		}
	}
}

package watki2;

public class IterationFactorialStrategy implements FactorialStrategy {

	@Override
	public long factorial(long number) {
		long result = 1;
		for (int i = 2; i <= number; i++) {
			result *= i;
		}
		return result;

	}
}

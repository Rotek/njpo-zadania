package watki2;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class FactorialThread extends Thread {
	private FactorialStrategy factorialStrategy;
	private JTextField textField;
	private long number;

	public FactorialThread(FactorialStrategy factorialStrategy, JTextField textField, long number) {
		super();
		this.factorialStrategy = factorialStrategy;
		this.textField = textField;
		this.number = number;
	}

	public long factorial(long number) {
		return factorialStrategy.factorial(number);
	}

	@Override
	public void run() {
		super.run();
		long startTime = System.currentTimeMillis();
		factorial(number);
		long endTime = System.currentTimeMillis();
		textField.setText(endTime - startTime + "");
	}

}

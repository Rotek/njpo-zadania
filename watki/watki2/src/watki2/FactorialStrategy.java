package watki2;

public interface FactorialStrategy {
	public long factorial(long number);
}

package watki2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainJFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnLicz;
	private FactorialThread factorialThreadRecursive;
	private FactorialThread factorialThreadIteration;
	private JButton btnNieChceMi;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJFrame frame = new MainJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 242, 255);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblWyliczSilnieZ = new JLabel("Wylicz silnie z");
		lblWyliczSilnieZ.setBounds(10, 11, 87, 14);
		contentPane.add(lblWyliczSilnieZ);

		JLabel lblCzasRekurencyjnie = new JLabel("Czas rekurencyjnie");
		lblCzasRekurencyjnie.setBounds(10, 83, 114, 14);
		contentPane.add(lblCzasRekurencyjnie);

		textField = new JTextField();
		textField.setBounds(10, 36, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(10, 108, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblCzasIteracyjnie = new JLabel("Czas iteracyjnie");
		lblCzasIteracyjnie.setBounds(10, 142, 114, 14);
		contentPane.add(lblCzasIteracyjnie);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(10, 167, 86, 20);
		contentPane.add(textField_2);

		btnNieChceMi = new JButton("nie chce mi sie czekac");
		btnNieChceMi.setEnabled(false);
		btnNieChceMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				factorialThreadIteration.stop();
				factorialThreadRecursive.stop();
			}
		});
		btnNieChceMi.setBounds(10, 198, 194, 23);
		contentPane.add(btnNieChceMi);

		btnLicz = new JButton("licz");
		btnLicz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text = textField.getText();
				try {
					long number = Long.parseLong(text);
					startCompetition(number);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(contentPane, "it's not a number");
				}

			}
		});
		btnLicz.setBounds(115, 35, 89, 23);
		contentPane.add(btnLicz);
	}

	private void startCompetition(long number) {
		factorialThreadRecursive = new FactorialThread(new RecursiveFactorialStrategy(), textField_1, number);
		factorialThreadIteration = new FactorialThread(new IterationFactorialStrategy(), textField_2, number);
		factorialThreadRecursive.start();
		factorialThreadIteration.start();
		btnNieChceMi.setEnabled(true);
	}

}

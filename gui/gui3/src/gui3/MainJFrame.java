package gui3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainJFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJFrame frame = new MainJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 590, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnClickMe = new JButton("Click me!");
		btnClickMe.addMouseListener(new MouseAdapter() {
			 private int sideChanger = -1;
			
			 @Override
			public void mouseEntered(MouseEvent event) {
				 int newX = btnClickMe.getX() + event.getX() * sideChanger;
				 int newY = btnClickMe.getY() + event.getY() * sideChanger;
				 btnClickMe.setBounds(newX, newY, btnClickMe.getWidth(),
				 btnClickMe.getHeight());
				 sideChanger *= -1;
			}
		});
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		contentPane.add(btnClickMe);
	}

}

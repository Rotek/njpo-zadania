package gui2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainJFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJFrame frame = new MainJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 175);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
				textField = new JTextField();
				textField.setColumns(10);
				contentPane.add(textField);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JRadioButton radioButton = new JRadioButton("+");
		buttonGroup.add(radioButton);
		panel.add(radioButton);

		JRadioButton radioButton_1 = new JRadioButton("-");
		buttonGroup.add(radioButton_1);
		panel.add(radioButton_1);

		JRadioButton radioButton_2 = new JRadioButton("*");
		buttonGroup.add(radioButton_2);
		panel.add(radioButton_2);

		JRadioButton radioButton_3 = new JRadioButton("/");
		buttonGroup.add(radioButton_3);
		panel.add(radioButton_3);
		contentPane.add(panel);
		
				textField_1 = new JTextField();
				textField_1.setColumns(10);
				contentPane.add(textField_1);

		JButton button = new JButton("=");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				calculateValue(radioButton, radioButton_1, radioButton_2, radioButton_3);
			}

			
		});
		contentPane.add(button);

		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);
	}
	private void calculateValue(JRadioButton radioButton, JRadioButton radioButton_1, JRadioButton radioButton_2,
			JRadioButton radioButton_3) {
		double firstArg = 0d;
		double secondArg = 0d;
		try {
			firstArg = Double.parseDouble(textField.getText());
			secondArg = Double.parseDouble(textField_1.getText());

		} catch (NumberFormatException e) {
			textField_2.setText("err");
		}
		if (radioButton.isSelected()) {
			textField_2.setText(firstArg + secondArg + "");
		} else if (radioButton_1.isSelected()) {
			textField_2.setText(firstArg - secondArg + "");
		} else if (radioButton_2.isSelected()) {
			textField_2.setText(firstArg * secondArg + "");
		} else if (radioButton_3.isSelected()) {
			textField_2.setText(firstArg / secondArg + "");
		} else {
			textField_2.setText("err");
		}
	}
}

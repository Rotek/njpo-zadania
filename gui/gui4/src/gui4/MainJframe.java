package gui4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainJframe extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private OutputStreamWriter fileOut;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainJframe frame = new MainJframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainJframe() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				try {
					fileOut.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		try {
			fileOut = new OutputStreamWriter(new FileOutputStream("pesel.txt", true));
		} catch (FileNotFoundException e) {
			System.err.println("something worn with pesel.txt file");
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 323, 94);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnDodajDoPliku = new JButton("Dodaj do pliku");
		btnDodajDoPliku.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String pesel = textField.getText();
				if (!isValidPesel(pesel)) {
					JOptionPane.showMessageDialog(textField, "wadliwy pesel");
				} else {
					try {
						fileOut.write(pesel + "\r\n");
						fileOut.flush();
						JOptionPane.showMessageDialog(textField, "dodano");
					} catch (IOException e) {
						System.err.println("somerthing went worng when trying to write pesl into file");
					}
				}
			}
		});
		contentPane.add(btnDodajDoPliku);
	}

	public static boolean isValidPesel(String pesel) {
		int[] wagi = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

		int psize = pesel.length();
		if (psize != 11) {
			return false;
		}

		int sum = 0;
		int controlSum;
		try {
			controlSum = Integer.parseInt(pesel.substring(10, 11));
		} catch (NumberFormatException e) {
			return false;
		}
		for (int i = 0; i < 10; i++)
			sum += Integer.parseInt(pesel.substring(i, i + 1)) * wagi[i];

		return (sum + controlSum) % 10 == 0;
	}

}

package dekorator1;

import java.util.Scanner;

public class Runner {

	public static void main(String[] args) {
		System.out.println(numberOfLines("asdasd\n\n\n asdasd,\nAS\n"));
	}

	private static int numberOfLines(String s) {
		int lines = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\n') {
				lines++;
			}
		}
		return lines;
	}

}

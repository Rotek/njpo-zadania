package dekorator2;

public class Runner {
	public static void main(String[] args) {
		System.out.println(numberOfWords("asdasd	\n  asdasd AS         as\n"));
	}

	private static int numberOfWords(String s) {
		int lines = 0;
		boolean isAWord = false;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\n' || s.charAt(i) == '\t' || s.charAt(i) == ' ') {
				if (isAWord) {
					lines++;
				}
				isAWord = false;
			} else {
				isAWord = true;
			}
		}
		return lines;
	}

}

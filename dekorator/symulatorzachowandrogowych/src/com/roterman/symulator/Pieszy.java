package com.roterman.symulator;

public class Pieszy extends RoadUser {

	private static final int pedestrianSpeed =1;
	public Pieszy(char character, Point startingPoint, MovingStrategy movingStrategy) {
		super(pedestrianSpeed, character, startingPoint, movingStrategy);
	}

}

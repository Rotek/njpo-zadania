package com.roterman.symulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Runner {

	public static void main(String[] args) throws IOException, InterruptedException {
		MovingStrategy movingStrategy = new RandomMovingStrategy();
		List<RoadUser> roadUsers = new ArrayList<RoadUser>();

		roadUsers.add(new Pieszy('P', new Point(5, 5), movingStrategy));
		roadUsers.add(new Rower(new Pieszy('R', new Point(10, 10), movingStrategy)));
		roadUsers.add(new Samochod(new Pieszy('C', new Point(15, 15), movingStrategy)));

		System.out.println("wpisanie dowolnego tekstu posuwa akcje o 1 jednostke czasowa");

		Board board = new Board(new Point(20, 20), roadUsers);
		BoardRenderer boardRenderer = new CLIBoardRenderer();
		boardRenderer.render(board);

		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNext()) {
			scanner.nextLine();
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			board.move();
			boardRenderer.render(board);
			if (board.hasCollisionOccured()) {
				System.out.println("gameOver, some man crashed");
				break;
			}
		}
		scanner.close();
	}

}

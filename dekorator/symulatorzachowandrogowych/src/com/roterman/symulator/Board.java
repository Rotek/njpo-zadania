package com.roterman.symulator;

import java.util.ArrayList;
import java.util.List;

public class Board {

	private Point topRightCorner;
	private List<RoadUser> roadUsers = new ArrayList<RoadUser>();
	private boolean collisionOccured = false;

	public Board(Point topRightCorner, List<RoadUser> roadUsers) {
		this.topRightCorner = topRightCorner;
		this.roadUsers = roadUsers;
	}

	public void move() {
		for (RoadUser roadUser : roadUsers) {
			roadUser.move();
		}
		if (checkColisions()) {
			collisionOccured = true;
		} else {
			collisionOccured = false;
		}
	}

	private boolean isUserBeyondEdges(RoadUser roadUser) {
		Point userPosition = roadUser.getPosition();
		if (0 > userPosition.getX()) {
			roadUser.setPosition(new Point(0, userPosition.getY()));
			return true;
		}
		if (userPosition.getX() > topRightCorner.getX()) {
			roadUser.setPosition(new Point(topRightCorner.getX(), userPosition.getY()));
			return true;
		}
		if (0 > userPosition.getY()) {
			return true;
		}
		if (userPosition.getY() > topRightCorner.getY()) {
			roadUser.setPosition(new Point(userPosition.getX(), topRightCorner.getY()));
			return true;
		}
		return false;
	}

	private boolean checkColisions() {
		for (RoadUser roadUser : roadUsers) {
			if (isUserBeyondEdges(roadUser)) {
				return true;
			}
			for (RoadUser roadUser2 : roadUsers) {
				if (roadUser == roadUser2) {
					continue;
				}
				if (checkColision(roadUser, roadUser2)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkColision(RoadUser roadUser1, RoadUser roadUser2) {
		return roadUser1.getPosition().equals(roadUser2.getPosition());

	}

	public Point getTopRightCorner() {
		return topRightCorner;
	}

	public void setTopRightCorner(Point topRightCorner) {
		this.topRightCorner = topRightCorner;
	}

	public List<RoadUser> getRoadUsers() {
		return roadUsers;
	}

	public void setRoadUsers(List<RoadUser> roadUsers) {
		this.roadUsers = roadUsers;
	}

	public boolean hasCollisionOccured() {
		return collisionOccured;
	}

}

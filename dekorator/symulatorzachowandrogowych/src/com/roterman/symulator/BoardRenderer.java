package com.roterman.symulator;

public interface BoardRenderer {

	public void render(Board board);
}

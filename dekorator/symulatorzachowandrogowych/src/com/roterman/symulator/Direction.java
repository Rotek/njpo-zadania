package com.roterman.symulator;

public enum Direction {
	SOUTH(new Point(0, -1)), NORTH(new Point(0, 1)), WEST(new Point(-1, 0)), EAST(
			new Point(1, 0));

	private Point coordinatesFromZeroPoint;

	private Direction(Point coordinatesFromZeroPoint) {
		this.coordinatesFromZeroPoint = coordinatesFromZeroPoint;
	}

	public Point getCoordinatesFromZeroPoint() {
		return coordinatesFromZeroPoint;
	}
}

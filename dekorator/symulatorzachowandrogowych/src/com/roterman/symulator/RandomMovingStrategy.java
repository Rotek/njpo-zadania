package com.roterman.symulator;

import java.util.Random;

public class RandomMovingStrategy implements MovingStrategy {

	private Random random = new Random();

	public RandomMovingStrategy() {
	}

	@Override
	public Direction move() {
		int randomizedInt = random.nextInt(4);
		return Direction.values()[randomizedInt];
	}

}

package com.roterman.symulator;

public interface MovingStrategy {

	public Direction move();
}

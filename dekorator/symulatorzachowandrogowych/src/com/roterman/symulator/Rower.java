package com.roterman.symulator;

public class Rower extends RoadUser {

	private static final int BIKESPEED = 2;

	public Rower(RoadUser roadUser) {
		super(BIKESPEED, roadUser.getCharacter(), roadUser.getPosition(), roadUser.getMovingStrategy());
		this.setDecorated(roadUser);
	}

}

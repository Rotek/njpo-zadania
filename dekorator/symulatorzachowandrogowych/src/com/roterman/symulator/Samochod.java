package com.roterman.symulator;

public class Samochod extends RoadUser {
	private static final int CARSPEED = 4;

	public Samochod(RoadUser roadUser) {
		super(CARSPEED, roadUser.getCharacter(), roadUser.getPosition(), roadUser.getMovingStrategy());
		this.setDecorated(roadUser);
	}
}

package com.roterman.symulator;

public abstract class RoadUser {

	private RoadUser decorated;
	private int tilesPerSecond;
	private char character;
	private Point position;
	private MovingStrategy movingStrategy;

	public RoadUser(int tilesPerSecond, char character, Point startingPoint, MovingStrategy movingStrategy) {
		this.tilesPerSecond = tilesPerSecond;
		this.character = character;
		position = startingPoint;
		this.movingStrategy = movingStrategy;
	}

	public void move() {
		for (int i = 0; i < tilesPerSecond; i++)
			moveByOneTile();
	}

	public RoadUser(RoadUser roadUser) {
		decorated = roadUser;
	}

	private void moveByOneTile() {
		Direction direction = movingStrategy.move();
		position = position.add(direction.getCoordinatesFromZeroPoint());
	}

	public int getTilesPerSecond() {
		return tilesPerSecond;
	}

	public void setTilesPerSecond(int tilesPerSecond) {
		this.tilesPerSecond = tilesPerSecond;
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public MovingStrategy getMovingStrategy() {
		return movingStrategy;
	}

	public void setMovingStrategy(MovingStrategy movingStrategy) {
		this.movingStrategy = movingStrategy;
	}

	public RoadUser getDecorated() {
		return decorated;
	}

	public void setDecorated(RoadUser decorated) {
		this.decorated = decorated;
	}

}

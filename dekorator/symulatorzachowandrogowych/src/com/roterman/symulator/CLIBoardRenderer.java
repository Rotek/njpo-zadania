package com.roterman.symulator;

public class CLIBoardRenderer implements BoardRenderer {

	@Override
	public void render(Board board) {
		char[][] znaki;
		znaki = new char[board.getTopRightCorner().getX()+1][board.getTopRightCorner().getY()+1];
		for (int i = 0; i < znaki.length; i++) {
			for (int j = 0; j < znaki[i].length; j++) {
				znaki[i][j] = '_';
			}
		}
		for (RoadUser roadUser : board.getRoadUsers()) {
			znaki[roadUser.getPosition().getX()][roadUser.getPosition().getY()] = roadUser.getCharacter();
		}
		for (char[] row : znaki) {
			System.out.println(row);
		}
	}

}

package testy2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class RandomGeneratorTest {
	@Test(timeout = 20000)
	public void performanceTest() throws Exception {
		new RandomGenerator().get1e7SortedRandomNumbers();
	}

	@Test
	public void sizeTest() throws Exception {
		List<Integer> result = new RandomGenerator().get1e7SortedRandomNumbers();
		int expectedSize = 10000000;
		assertEquals(expectedSize, result.size());
	}
}

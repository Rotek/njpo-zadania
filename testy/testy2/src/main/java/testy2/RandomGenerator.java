package testy2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomGenerator {
	public static final Random RANDOM = new Random();

	public List<Integer> get1e7SortedRandomNumbers() {
		return getSortedRandomNumbers(10000000);
	}

	private List<Integer> getSortedRandomNumbers(int length) {
		List<Integer> numbers = new ArrayList<Integer>(length);
		for (int i = 0; i < length; i++) {
			numbers.add(RANDOM.nextInt());
		}
		Collections.sort(numbers);
		return numbers;
	}
}

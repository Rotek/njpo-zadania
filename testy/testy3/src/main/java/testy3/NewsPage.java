package testy3;

public class NewsPage extends HTMLPage {
	private StringBuilder content = new StringBuilder();

	@Override
	public String getPage() {
		return appendDefaultTags(content.toString());
	}

	public void addNews(String title, String content) {
		this.content.append("<h1>" + title + "</h1>\n" + "<p>" + content + "</p>\n");
	}
}

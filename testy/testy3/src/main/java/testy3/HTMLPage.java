package testy3;

public abstract class HTMLPage {
	private static final String HTMLBeginningTags = "<!DOCTYPE html><html><head><title>";
	private static final String hTMLBodyStartingTags = "</title></head><body>";
	private static final String HTMLENDINGTAGS = "</body></html>";

	private String title = "default title";

	public abstract String getPage();

	protected String appendDefaultTags(String content) {
		return HTMLBeginningTags + title + hTMLBodyStartingTags + content + HTMLENDINGTAGS;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}

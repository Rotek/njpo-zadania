package testy3;

import java.text.DecimalFormat;
import java.util.Random;

public class HTMLPagesFactory {
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final Random random = new Random();

	public HTMLPage getHTMLPage(HTMLPageTypes type) {
		switch (type) {
		case GALERY:
			GalleryPage galleryPage = new GalleryPage();
			galleryPage.addImage(
					"http://www.quickmeme.com/img/98/98d2795b917bcfb3249672bad615294bf0dee1240947560c1ae4ac8ed42eb2be.jpg");
			galleryPage.addImage("http://cdn.meme.am/instances/500x/54855582.jpg");
			return galleryPage;
		case INFO:
			InfoPage infoPage = new InfoPage();
			infoPage.addInfo(randomString(300));
			return infoPage;
		case NEWS:
			NewsPage newsPage = new NewsPage();
			newsPage.addNews(randomString(20), randomString(300));
			newsPage.addNews(randomString(20), randomString(300));
			newsPage.addNews(randomString(20), randomString(300));
			newsPage.addNews(randomString(20), randomString(300));
			return newsPage;
		case CONTACT:
			ContactPage contactPage = new ContactPage();
			contactPage.addContact(randomString(5), randomString(10), randomPhoneNumber());
			contactPage.addContact(randomString(5), randomString(10), randomPhoneNumber());
			contactPage.addContact(randomString(5), randomString(10), randomPhoneNumber());
			contactPage.addContact(randomString(5), randomString(10), randomPhoneNumber());
			return contactPage;
		}
		return null;
	}

	private String randomString(int len) {
		StringBuilder stringBuilder = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			stringBuilder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
		return stringBuilder.toString();
	}

	private String randomPhoneNumber() {
		int num1 = (random.nextInt(7) + 1) * 100 + (random.nextInt(8) * 10) + random.nextInt(8);
		int num2 = random.nextInt(743);
		int num3 = random.nextInt(10000);

		DecimalFormat df3 = new DecimalFormat("000");
		DecimalFormat df4 = new DecimalFormat("0000");

		return df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
	}
}

package testy3;

public class InfoPage extends HTMLPage {
	private StringBuilder content = new StringBuilder();

	@Override
	public String getPage() {
		return appendDefaultTags(content.toString());
	}

	public void addInfo(String info) {
		content.append(info + "\n");
	}
}

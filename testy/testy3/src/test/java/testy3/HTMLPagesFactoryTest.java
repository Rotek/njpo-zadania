package testy3;

import static org.junit.Assert.assertThat;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

public class HTMLPagesFactoryTest {
	@Test
	public void getProperPage() throws Exception {
		HTMLPagesFactory factory = new HTMLPagesFactory();
		assertThat(factory.getHTMLPage(HTMLPageTypes.CONTACT), IsInstanceOf.instanceOf(ContactPage.class));
		assertThat(factory.getHTMLPage(HTMLPageTypes.GALERY), IsInstanceOf.instanceOf(GalleryPage.class));
		assertThat(factory.getHTMLPage(HTMLPageTypes.NEWS), IsInstanceOf.instanceOf(NewsPage.class));
		assertThat(factory.getHTMLPage(HTMLPageTypes.INFO), IsInstanceOf.instanceOf(InfoPage.class));
	}
}

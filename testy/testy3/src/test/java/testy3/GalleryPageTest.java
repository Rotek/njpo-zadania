package testy3;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GalleryPageTest {
	@Test
	public void containsImage() {
		GalleryPage page = new GalleryPage();
		page.addImage("url");
		assertTrue(page.getPage().contains("<img src =\"url\">"));
	}

	@Test
	public void containsImages() {
		GalleryPage page = new GalleryPage();
		String[] urls = { "url1", "url2", "url3" };
		for (String url : urls) {
			page.addImage(url);
		}
		for (String url : urls) {
			assertTrue(page.getPage().contains("<img src =\"" + url + "\">"));
		}
	}
}

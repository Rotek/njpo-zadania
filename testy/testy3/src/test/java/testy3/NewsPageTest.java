package testy3;

import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

public class NewsPageTest {
	@Test
	public void containsNews() {
		NewsPage page = new NewsPage();
		String title = "expected title";
		String content = "expected content";
		page.addNews(title, content);
		assertTrue(page.getPage().contains("<h1>" + title + "</h1>\n" + "<p>" + content + "</p>\n"));
	}

	@Test
	public void containsMultipleNews() {
		NewsPage page = new NewsPage();
		String[] titles = { "title1", "title2", "title3" };
		String[] contents = { "content1", "content2", "content3" };
		for (int i = 0; i < contents.length; i++) {
			page.addNews(titles[i], contents[i]);
		}
		for (int i = 0; i < contents.length; i++) {
			assertTrue(page.getPage().contains("<h1>" + titles[i] + "</h1>\n" + "<p>" + contents[i] + "</p>\n"));
		}
	}
}

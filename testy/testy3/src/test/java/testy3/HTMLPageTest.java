package testy3;

import static org.junit.Assert.*;

import org.junit.Test;

public class HTMLPageTest {

	@Test
	public void containsTitle() throws Exception {
		HTMLPage page = new GalleryPage();
		String title = "expected title";
		page.setTitle(title);
		assertTrue(page.getPage().contains(title));
	}
	
	@Test
	public void containsHTMLTags() throws Exception {
		HTMLPage page = new GalleryPage();
		assertTrue(page.getPage().contains("<!DOCTYPE html><html><head><title>"));
		assertTrue(page.getPage().contains("</title></head><body>"));
		assertTrue(page.getPage().contains("</body></html>"));
	}

}

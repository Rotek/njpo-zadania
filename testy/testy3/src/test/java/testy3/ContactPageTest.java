package testy3;

import static org.junit.Assert.*;

import org.junit.Test;

public class ContactPageTest {

	@Test
	public void containsContact(){
		ContactPage page = new ContactPage();
		page.addContact("name", "surname", "telephoneNumber");
		assertTrue(page.getPage().contains("<pre>name surname telephoneNumber\n</pre>"));
	}
	@Test
	public void containsContacts() {
		ContactPage page = new ContactPage();
		page.addContact("name", "surname", "telephoneNumber");
		page.addContact("name2", "surname2", "telephoneNumber2");
		assertTrue(page.getPage().contains("<pre>name surname telephoneNumber\n"
				+ "name2 surname2 telephoneNumber2\n</pre>"));
	}
}

package testy3;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class InfoPageTest {
	@Test
	public void containsInfo() {
		InfoPage page = new InfoPage();
		page.addInfo("info");
		assertTrue(page.getPage().contains("info"));
	}

	@Test
	public void containsInfos() {
		InfoPage page = new InfoPage();
		String[] infos = { "info1", "info2", "info3" };
		for (String info : infos) {
			page.addInfo(info);
		}
		for (String info : infos) {
			assertTrue(page.getPage().contains(info));
		}
	}
}

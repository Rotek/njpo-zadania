import java.util.ArrayList;
import java.util.List;

public class QuadraticEquation {

	private double a;
	private double b;
	private double c;

	public QuadraticEquation(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	private int getNumberOfSquares() {
		double delta = getDelta();
		return ((int) Math.signum(delta)) + 1;
	}

	private double getDelta() {
		return b * b - (4 * a * c);
	}

	public List<Double> calculateSquares() {
		double delta = getDelta();
		List<Double> squares = new ArrayList<Double>(2);
		if (getNumberOfSquares() > 0) {
			double square1 = (-b - Math.sqrt(delta)) / 2 * a;
			double square2 = (-b + Math.sqrt(delta)) / 2 * a;
			squares.add(square1);
			squares.add(square2);
		}
		return squares;
	}
}

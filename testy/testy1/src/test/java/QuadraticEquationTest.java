import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.List;

import org.junit.Test;

public class QuadraticEquationTest {

	@Test
	public void testSolve() {
		double a = 1;
		double b = 3;
		double c = -4;
		Double[] expectedResult = { -4d, 1d };
		List<Double> result = new QuadraticEquation(a, b, c).calculateSquares();

		assertThat(result.toArray(new Double[2]), is(expectedResult));

	}

	@Test
	public void testSolveNoSquares() {
		double a = 1;
		double b = 1;
		double c = 4;
		List<Double> result = new QuadraticEquation(a, b, c).calculateSquares();

		assertThat(result.isEmpty(), is(true));

	}
}

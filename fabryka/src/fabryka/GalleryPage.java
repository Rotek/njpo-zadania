package fabryka;

public class GalleryPage extends HTMLPage {
	private StringBuilder content = new StringBuilder();

	@Override
	public String getPage() {
		return appendDefaultTags(content.toString());
	}

	public void addImage(String url) {
		content.append("<img src =\"" + url + "\">");
	}

}

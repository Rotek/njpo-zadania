package fabryka;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Random;

public class Runner {
	public static void main(String[] args) throws IOException {
		HTMLPagesFactory htmlPagesFactory = new HTMLPagesFactory();
		Random random = new Random();
		int randomizedHTMLPageTypeNumber = random.nextInt(HTMLPageTypes.values().length);
		HTMLPageTypes randomizedHTMLPageType = HTMLPageTypes.values()[randomizedHTMLPageTypeNumber];

		String htmlCode = htmlPagesFactory.getHTMLPage(randomizedHTMLPageType).getPage();

		System.out.println(randomizedHTMLPageType.name() + ".html site has been randomly generated");
		try (OutputStreamWriter outToFile = new OutputStreamWriter(
				new FileOutputStream(randomizedHTMLPageType.name() + ".html"))) {
			outToFile.write(htmlCode);
		}
	}
}

package fabryka;

public class ContactPage extends HTMLPage {
	private StringBuilder content = new StringBuilder();

	@Override
	public String getPage() {
		return appendDefaultTags("<pre>" + content.toString() + "</pre>");
	}

	public void addContact(String name, String surname, String telephoneNumber) {
		content.append(name + " " + surname + " " + telephoneNumber + "\n");
	}
}

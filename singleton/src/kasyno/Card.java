package kasyno;

public enum Card {
	DWA(2, "2"),
	TRZY(3, "3"),
	CZTERY(4, "4"),
	PIEC(5, "5"),
	WSZESC(6, "6"),
	SIEDEM(7, "7"),
	OSIEM(8, "8"),
	DZIEWIEC(9, "9"),
	DZIESIEC(10, "10"),
	JOPEK(10, "J"),
	DAMA(10, "D"),
	KROL(10, "K"),
	AS(11, "AS");

	private final int points;
	private final String symbol;

	private Card(int value, String symbol) {
		this.points = value;
		this.symbol = symbol;
	}

	public int getPoints() {
		return points;
	}

	public String getSymbol() {
		return symbol;
	}

}

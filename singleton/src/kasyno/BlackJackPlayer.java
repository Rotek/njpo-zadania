package kasyno;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BlackJackPlayer implements Player {

	private static final List<Card> CARDS = Collections.unmodifiableList(Arrays.asList(Card.values()));
	private static final Random RANDOM = new Random();
	private static final int ONWINSTAKEMULTIPLIER = 2;
	private static final int PONITSTOWIN = 21;

	private int reward = 0;
	private int stake;
	private List<Card> playerCards = new ArrayList<>();
	private List<Card> croupierCards = new ArrayList<>();

	@Override
	public void play() {
		System.out.println("witaj, Ty i krupier otrzymujecie 2 karty");
		playerCards.add(getRandomCard());
		playerCards.add(getRandomCard());
		croupierCards.add(getRandomCard());
		croupierCards.add(getRandomCard());
		printDeck();

		Scanner scanner = Runner.scanner;
		while (true) {
			System.out.println("p - pass, cokolowiek aby pobrac nastepna karte");
			String playerDecision = scanner.nextLine();
			String croypierDecision = pointsOfCards(croupierCards) > 16 ? "p" : "a";
			if (!playerDecision.equals("p")) {
				playerCards.add(getRandomCard());
			}
			if (!croypierDecision.equals("p")) {
				croupierCards.add(getRandomCard());
			}
			printDeck();
			if (pointsOfCards(croupierCards) > PONITSTOWIN) {
				reward = stake * ONWINSTAKEMULTIPLIER;
				System.out.println("wygrales! " + reward);
				printFullDeck();
				return;
			} else if (pointsOfCards(playerCards) > PONITSTOWIN) {
				System.out.println("przegrales");
				printFullDeck();
				return;
			} else if (croypierDecision.equals("p") && playerDecision.equals("p")) {
				if (pointsOfCards(playerCards) > pointsOfCards(croupierCards)) {
					reward = stake * ONWINSTAKEMULTIPLIER;
					System.out.println("wygrales! " + reward);
					printFullDeck();
					return;
				} else {
					System.out.println("przegrales");
					printFullDeck();
					return;
				}
			}
		}
	}

	@Override
	public void setStake(int stake) {
		this.stake = stake;
	}

	@Override
	public int getReward() {
		return reward;
	}

	private Card getRandomCard() {

		return CARDS.get(RANDOM.nextInt(CARDS.size()));
	}

	private void printDeck() {
		System.out.println("your cards");
		playerCards.stream().forEachOrdered(card -> System.out.print(card.getSymbol() + " "));
		System.out.println("\ncroupier cards");
		System.out.println(croupierCards.get(0).getSymbol());
		System.out.println();
	}

	private void printFullDeck() {
		System.out.println("your cards");
		playerCards.stream().forEachOrdered(card -> System.out.print(card.getSymbol() + " "));
		System.out.println("\ncroupier cards");
		croupierCards.stream().forEachOrdered(card -> System.out.print(card.getSymbol() + " "));
		System.out.println();
	}

	private int pointsOfCards(List<Card> cards) {
		int points = 0;
		int ASammount = 0;
		for (Card card : cards) {
			if (card.equals(Card.AS)) {
				ASammount++;
			}
			points += card.getPoints();
		}
		while (ASammount > 0 && points > PONITSTOWIN) {
			points -= 10;
			ASammount--;
		}
		return points;
	}
}

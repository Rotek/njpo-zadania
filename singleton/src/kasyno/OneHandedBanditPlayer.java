package kasyno;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class OneHandedBanditPlayer implements Player {

	public static final List<Character> CHARACTERS = Arrays.asList('a', 'b', 'c', 'd', '7');
	public static final Random Random = new Random();
	private static final int ONWINSTAKEMULTIPLIER = 2;

	private int stake;
	private int reward;

	@Override
	public void play() {
		Character[] pickedCharacters = pickCharacters();
		System.out.println(pickedCharacters[0] + " " + pickedCharacters[1] + " " + pickedCharacters[2]);
		if (pickedCharacters[0].equals(pickedCharacters[1]) && pickedCharacters[1].equals(pickedCharacters[2])) {
			reward = stake * ONWINSTAKEMULTIPLIER;
			System.out.println("gratulacje wyglrales " + reward);
			return;
		} else {
			System.out.println("niestety przegrales");
		}

	}

	private Character[] pickCharacters() {
		Character[] pickedCharacters = new Character[3];
		for (int i = 0; i < pickedCharacters.length; i++) {
			pickedCharacters[i] = CHARACTERS.get(Random.nextInt(CHARACTERS.size()));
		}
		return pickedCharacters;
	}

	@Override
	public void setStake(int stake) {
		this.stake = stake;
	}

	@Override
	public int getReward() {
		return reward;
	}

}

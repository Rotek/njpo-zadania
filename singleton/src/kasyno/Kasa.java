package kasyno;

public enum Kasa {

	INSTANCE(0);
	private int money;

	private Kasa(int money) {
		this.money = money;
	}

	public void addMoney(int ammount) {
		this.money += ammount;
	}

	public int getMoney() {
		return money;
	}

}

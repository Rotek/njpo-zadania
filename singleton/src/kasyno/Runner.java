package kasyno;

import java.util.Scanner;

public class Runner {

	static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("bonus powitalny 5000 jednostek");
		Kasa.INSTANCE.addMoney(5000);
		while (Kasa.INSTANCE.getMoney() > 0) {
			System.out.println("1 blackjack, 2 jednoreki bandyta");
			String choice = scanner.nextLine();
			while (!(choice.equals("1") || choice.equals("2"))) {
				System.out.println("try again my friend");
				choice = scanner.nextLine();
			}
			Player player = null;
			switch (choice) {
			case "1":
				player = new BlackJackPlayer();
				break;
			case "2":
				player = new OneHandedBanditPlayer();
				break;
			}
			System.out.println("wybierz stawke");
			int stawka = scanner.nextInt();
			scanner.nextLine();
			while (Kasa.INSTANCE.getMoney() < stawka || stawka < 1) {
				System.out.println("zbyt wysoka stawka, nie masz tyle");
				stawka = scanner.nextInt();
				scanner.nextLine();
			}
			Kasa.INSTANCE.addMoney(-stawka);
			player.setStake(stawka);
			player.play();
			int result = player.getReward();
			Kasa.INSTANCE.addMoney(result);
			System.out.println("stan twojego konta to " + Kasa.INSTANCE.getMoney());
		}
		scanner.close();
		System.out.println("sorry zbankrutowales");
	}

}

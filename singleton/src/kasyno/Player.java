package kasyno;

public interface Player {

	public void play();

	public void setStake(int stake);

	public int getReward();
}
